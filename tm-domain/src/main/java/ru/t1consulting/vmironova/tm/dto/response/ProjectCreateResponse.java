package ru.t1consulting.vmironova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectCreateResponse extends AbstractProjectResponse {

    public ProjectCreateResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
