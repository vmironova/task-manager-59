package ru.t1consulting.vmironova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.configuration.ServerConfiguration;
import ru.t1consulting.vmironova.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @Nullable
    private static IPropertyService PROPERTY_SERVICE;

    @BeforeClass
    public static void setUp() throws Exception {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
    }

    @Test
    @Ignore
    public void getApplicationVersion() {
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationVersion());
    }

    @Test
    @Ignore
    public void getAuthorName() {
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorName());
    }

    @Test
    @Ignore
    public void getAuthorEmail() {
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorEmail());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerPort());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerHost());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionTimeout());
    }

    @Test
    public void getDBUser() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDBUser());
    }

    @Test
    public void getDBPassword() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDBPassword());
    }

    @Test
    public void getDBUrl() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDBUrl());
    }

    @Test
    public void getDBDriver() {
        Assert.assertNotNull(PROPERTY_SERVICE.getDBDriver());
    }

}
