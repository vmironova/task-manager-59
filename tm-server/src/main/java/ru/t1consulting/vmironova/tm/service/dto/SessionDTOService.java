package ru.t1consulting.vmironova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1consulting.vmironova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1consulting.vmironova.tm.api.service.dto.ISessionDTOService;
import ru.t1consulting.vmironova.tm.dto.model.SessionDTO;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, ISessionDTORepository> implements ISessionDTOService {

    @Nullable
    @Autowired
    private ISessionDTORepository repository;

    @Nullable
    protected ISessionDTORepository getRepository() {
        return repository;
    }

}
