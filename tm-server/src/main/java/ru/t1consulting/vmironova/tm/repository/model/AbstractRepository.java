package ru.t1consulting.vmironova.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.api.FieldConstants;
import ru.t1consulting.vmironova.tm.api.repository.model.IRepository;
import ru.t1consulting.vmironova.tm.comparator.CreatedComparator;
import ru.t1consulting.vmironova.tm.comparator.StatusComparator;
import ru.t1consulting.vmironova.tm.model.AbstractModel;
import ru.volnenko.component.util.GenericUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @SuppressWarnings("unchecked")
    protected Class<M> getEntityClass() {
        return (Class<M>) GenericUtil.getClassFirst(this.getClass());
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return FieldConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return FieldConstants.COLUMN_STATUS;
        else return FieldConstants.COLUMN_NAME;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() throws Exception {
        @Nullable final List<M> models = findAll();
        if (models == null) return;
        for (@NotNull final M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getEntityClass()).getResultList();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getEntityClass());
        @NotNull final Root<M> from = criteriaQuery.from(getEntityClass());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        return entityManager.find(getEntityClass(), id);
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) throws Exception {
        entityManager.remove(entityManager.merge(model));
    }

    @Override
    public void update(@NotNull final M model) throws Exception {
        entityManager.merge(model);
    }

}
