package ru.t1consulting.vmironova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
