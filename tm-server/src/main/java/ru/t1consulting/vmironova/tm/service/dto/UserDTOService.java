package ru.t1consulting.vmironova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.vmironova.tm.api.repository.dto.IUserDTORepository;
import ru.t1consulting.vmironova.tm.api.service.IPropertyService;
import ru.t1consulting.vmironova.tm.api.service.dto.IProjectDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.ITaskDTOService;
import ru.t1consulting.vmironova.tm.api.service.dto.IUserDTOService;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.enumerated.Role;
import ru.t1consulting.vmironova.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.EmailEmptyExceprion;
import ru.t1consulting.vmironova.tm.exception.field.IdEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.LoginEmptyException;
import ru.t1consulting.vmironova.tm.exception.field.PasswordEmptyException;
import ru.t1consulting.vmironova.tm.exception.user.ExistsEmailException;
import ru.t1consulting.vmironova.tm.exception.user.ExistsLoginException;
import ru.t1consulting.vmironova.tm.exception.user.RoleEmptyException;
import ru.t1consulting.vmironova.tm.util.HashUtil;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService<UserDTO, IUserDTORepository> implements IUserDTOService {

    @Nullable
    @Autowired
    private IUserDTORepository repository;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    protected IUserDTORepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyExceprion();
        return repository.findByEmail(email);
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        return (repository.findByLogin(login) != null);
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) return false;
        return (repository.findByEmail(email) != null);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.clear(userId);
        projectService.clear(userId);
        remove(user);
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
    }

    @Override
    @Transactional
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        repository.update(user);
    }

}
