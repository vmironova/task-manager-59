package ru.t1consulting.vmironova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.vmironova.tm.api.repository.model.ITaskRepository;
import ru.t1consulting.vmironova.tm.api.service.model.IProjectService;
import ru.t1consulting.vmironova.tm.api.service.model.ITaskService;
import ru.t1consulting.vmironova.tm.api.service.model.IUserService;
import ru.t1consulting.vmironova.tm.enumerated.Status;
import ru.t1consulting.vmironova.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.vmironova.tm.exception.field.*;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @Autowired
    private ITaskRepository repository;

    @Nullable
    protected ITaskRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.update(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public Task create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setUser(userService.findOneById(userId));
        repository.add(userId, task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userService.findOneById(userId));
        repository.add(userId, task);
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.update(userId, task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String projectId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setProject(projectService.findOneById(projectId));
        repository.update(userId, task);
    }

}
